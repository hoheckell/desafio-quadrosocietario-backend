<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240316182012 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE empresas_socios (empresa_id INT NOT NULL, socio_id INT NOT NULL, PRIMARY KEY(empresa_id, socio_id))');
        $this->addSql('CREATE INDEX IDX_BC479A66521E1991 ON empresas_socios (empresa_id)');
        $this->addSql('CREATE INDEX IDX_BC479A66DA04E6A9 ON empresas_socios (socio_id)');
        $this->addSql('ALTER TABLE empresas_socios ADD CONSTRAINT FK_BC479A66521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE empresas_socios ADD CONSTRAINT FK_BC479A66DA04E6A9 FOREIGN KEY (socio_id) REFERENCES socio (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE empresas_socios DROP CONSTRAINT FK_BC479A66521E1991');
        $this->addSql('ALTER TABLE empresas_socios DROP CONSTRAINT FK_BC479A66DA04E6A9');
        $this->addSql('DROP TABLE empresas_socios');
    }
}
