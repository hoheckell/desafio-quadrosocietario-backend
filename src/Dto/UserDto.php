<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UserDto
{ 

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public ?string $id;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $email;

    public function __construct($name,$email,$id=null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }
}
