<?php

namespace App\Dto;

use App\Entity\Empresa;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class SocioDto
{

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public ?string $id;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public ?string $email;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public ?string $cpf;

    /**
     * @var EmpresaDto[]
     */
    public ?array $empresas;

    public function __construct($name, $cpf, $email, $id = null)
    {
        $this->id = $id;
        $this->cpf = $cpf;
        $this->name = $name;
        $this->email = $email;
    } 
}
