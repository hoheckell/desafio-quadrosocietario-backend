<?php

namespace App\Dto;

use App\Entity\Socio;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class EmpresaDto
{ 

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public ?string $id;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $razao_social;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public ?string $nome_fantasia;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public ?string $cnpj; 
    
    /**
     * @var SocioDto[]
     */
    public ?array $socios; 
    
    public function __construct($cnpj,$nome_fantasia,$razao_social,$id=null)
    {
        $this->id = $id;
        $this->cnpj = $cnpj;
        $this->nome_fantasia = $nome_fantasia;
        $this->razao_social = $razao_social; 
    }
}
