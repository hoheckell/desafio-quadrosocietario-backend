<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Delete;
use App\Dto\SocioDto;
use App\Repository\SocioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: SocioRepository::class)]
#[ApiResource(operations: [
    new GetCollection(
        routeName: 'list_socios',
    ),
    new Post(
        routeName: 'add_socio',
    ),
    new Put(
        routeName: 'update_socio',
    ),
    new Delete(
        routeName: 'delete_socio',
    ),
    new Get(
        routeName: 'get_socio',
    )
]
)]
class Socio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\Column(length: 100, unique: true)]
    private ?string $email = null;

    #[ORM\Column(length: 14, unique: true)]
    private ?string $cpf = null; 

    #[ORM\ManyToMany(targetEntity: Empresa::class, mappedBy:"socios", fetch:"EAGER")]
    private ?Collection $empresas;

    public function __construct()
    {
        $this->empresas = new ArrayCollection(); 
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): static
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * @return Collection|Empresa[]
     */
    public function getEmpresas(): ?Collection
    {
        return $this->empresas;
    }

    public function addEmpresa(Empresa $empresa): static
    {
        if (!$this->empresas->contains($empresa)) {
            $this->empresas->add($empresa);
            $empresa->addSocio($this);
        }

        return $this;
    }

    public function removeEmpresa(Empresa $empresa): static
    {
        if ($this->empresas->removeElement($empresa)) {
            // set the owning side to null (unless already changed)
            if ($empresa->getSocios()->contains($this)) {
                $empresa->removeSocio($this);
            }
        }

        return $this;
    }
    public function toDto(): SocioDto
    {  
        return new SocioDto(
            $this->name,
            $this->cpf,
            $this->email,
            $this->id
        );
    }
}
