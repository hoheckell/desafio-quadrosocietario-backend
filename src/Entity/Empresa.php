<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Dto\EmpresaDto;
use App\Repository\EmpresaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmpresaRepository::class)]
#[ApiResource(operations: [
    new GetCollection(
        routeName: 'list_empresas',
    ),
    new Post(
        routeName: 'add_empresa',
    ),
    new Post(
        routeName: 'associar',
    ),
    new Put(
        routeName: 'update_empresa',
    ),
    new Delete(
        routeName: 'delete_empresa',
    ),
    new Delete(
        routeName: 'desassociar',
    ),
    new Get(
        routeName:'get_empresa',
    )
]
)]
class Empresa
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $razao_social = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nome_fantasia = null;

    #[ORM\Column(length: 18, unique: true)]
    private ?string $cnpj = null; 

    #[ORM\ManyToMany(targetEntity: Socio::class, inversedBy:"empresas", fetch:"EAGER")]
    #[ORM\JoinTable(name:"empresas_socios")]
    private ?Collection $socios = null;

    public function __construct()
    {
        $this->socios = new ArrayCollection(); 
    } 
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRazaoSocial(): ?string
    {
        return $this->razao_social;
    }

    public function setRazaoSocial(string $razao_social): static
    {
        $this->razao_social = $razao_social;

        return $this;
    }

    public function getNomeFantasia(): ?string
    {
        return $this->nome_fantasia;
    }

    public function setNomeFantasia(?string $nome_fantasia): static
    {
        $this->nome_fantasia = $nome_fantasia;

        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(string $cnpj): static
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * @return Collection<int, Socio>
     */
    public function getSocios(): ?Collection
    {
        return $this->socios;
    }

    public function addSocio(Socio $socio): static
    {
        if (!$this->socios->contains($socio)) {
            $this->socios->add($socio);
            $socio->addEmpresa($this);
        }

        return $this;
    }

    public function removeSocio(Socio $socio): static
    {
        if ($this->socios->removeElement($socio)) {
            // set the owning side to null (unless already changed)
            if ($socio->getEmpresas()->contains($this)) {
                $socio->removeEmpresa($this);
            }
        }

        return $this;
    }
    public function toDto(): EmpresaDto
    {
        return new EmpresaDto(
            $this->getCnpj(),
            $this->getNomeFantasia(),
            $this->getRazaoSocial(), 
            $this->getId(),
        );
    }

}
