<?php

namespace App\Controller;

use App\Dto\SocioDto;
use App\Repository\SocioRepository;
use App\Validator\Cpf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route("/api/socios")]
class SocioController extends AbstractController
{
    public function __construct(private ValidatorInterface $validator,private SocioRepository $socioRepository)
    {
    }

    #[Route('', name: 'list_socios', methods: ['GET'])]
    public function index(): Response
    {
        $response = new Response();
        $socios = $this->socioRepository->list();   
        if (!empty($socios)) {
            $response->setContent(json_encode($socios));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }
        
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'get_socio', methods: ['GET'])]
    public function show(int $id): Response
    {
        $response = new Response();
        $socio = $this->socioRepository->getOne($id);   
        if (!empty($socio)) {
            $response->setContent(json_encode($socio));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }
        
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('', name: 'add_socio', methods: ['POST'])]
    public function store(Request $request) : Response
    {
        $socio = $request->getPayload()->all();
        $response = new Response();
        
        $validation = $this->validateSocioData($socio);        
        if($validation->getStatusCode() == 400) {
            return $validation;
        }

        $newSocio = $this->socioRepository->addSocio($socio);
        if (!empty ($newSocio)) {
            $response->setStatusCode(201);
        } else {
            $response->setStatusCode(400);
        }

        $response->setContent(json_encode($newSocio));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'update_socio', methods: ['PUT'])]
    public function update(Request $request, int $id): Response
    {
        $response = new Response();
        $socio = $request->getPayload()->all(); 

        $this->socioRepository->updateSocio($socio, $id);

        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'delete_socio', methods: ['DELETE'])]
    public function destroy(int $id): Response
    {
        $response = new Response();
        $this->socioRepository->delete($id);
        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function validateSocioData(array $data): Response 
    {
        $response =  new Response();
        $constraints = new Assert\Collection([
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],
            'email' => [
                new Assert\Type('string'),
            ],
            'cpf' => [
                new Cpf(),
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ]
        ]);

        $errors = $this->validator->validate($data, $constraints);

        if (count($errors) > 0) { 
            $messages = [];
            foreach ($errors as $k => $v) { 
                $messages[$v->getPropertyPath()] = $v->getMessage();
            }
            $response->setContent(json_encode($messages));
            $response->setStatusCode(400);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return $response;
    }
}
