<?php

namespace App\Controller;

use App\Dto\EmpresaDto;
use App\Repository\EmpresaRepository;
use App\Validator\TestCnpj;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route("/api/empresas")]
class EmpresaController extends AbstractController
{
    public function __construct(private ValidatorInterface $validator, private EmpresaRepository $empresaRepository)
    {
    }
    #[Route('', name: 'list_empresas', methods: ['GET'])]
    public function index(): Response
    {
        $response = new Response();
        $empresas = $this->empresaRepository->list();
        if (!empty ($empresas)) {
            $response->setContent(json_encode($empresas));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'get_empresa', methods: ['GET'])]
    public function show(int $id): Response
    {
        $response = new Response();
        $empresa = $this->empresaRepository->getOne($id);
        if (!empty ($empresa)) {
            $response->setContent(json_encode($empresa));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{empresaId}/socios/{socioId}', name: 'desassociar', methods: ['DELETE'])]
    public function desassociar(int $empresaId, int $socioId): Response
    {
        $response = new Response();
        $this->empresaRepository->desassociar($empresaId, $socioId);
        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{empresaId}/socios/', name: 'associar', methods: ['POST'])]
    public function associar(int $empresaId, Request $request): Response
    {
        $response = new Response();
        $empresa = $this->empresaRepository->associar($empresaId, $request->getPayload()->all());
        if (!empty ($empresa)) {
            $response->setContent(json_encode($empresa));
            $response->setStatusCode(201);
        } else {
            $response->setStatusCode(204);
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('', name: 'add_empresa', methods: ['POST'])]
    public function store(Request $request): Response
    {
        $empresa = $request->getPayload()->all();
        $response = new Response();

        $validation = $this->validateEmpresaData($empresa);
        if ($validation->getStatusCode() == 400) {
            return $validation;
        }

        $newEmpresa = $this->empresaRepository->addEmpresa($empresa);
        if (!empty ($newEmpresa)) {
            $response->setStatusCode(201);
        } else {
            $response->setStatusCode(400);
        }

        $response->setContent(json_encode($newEmpresa));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'update_empresa', methods: ['PUT'])]
    public function update(Request $request, int $id): Response
    {
        $response = new Response();
        $empresa = $request->getPayload()->all();

        $this->empresaRepository->updateEmpresa($empresa, $id);

        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'delete_empresa', methods: ['DELETE'])]
    public function destroy(int $id): Response
    {
        $response = new Response();
        $this->empresaRepository->delete($id);
        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function validateEmpresaData(array $data): Response
    {
        $response = new Response();
        $constraints = new Assert\Collection([
            'razao_social' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],
            'nome_fantasia' => [
                new Assert\Type('string'),
            ],
            'cnpj' => [
                new TestCnpj(),
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ]
        ]);

        $errors = $this->validator->validate($data, $constraints);

        if (count($errors) > 0) {
            $messages = [];
            foreach ($errors as $k => $v) {
                $messages[$v->getPropertyPath()] = $v->getMessage();
            }
            $response->setContent(json_encode($messages));
            $response->setStatusCode(400);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return $response;
    }
}
