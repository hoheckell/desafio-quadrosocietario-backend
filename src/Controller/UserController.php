<?php

namespace App\Controller;

use App\Dto\UserDto;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[Route("/api/users")]
class UserController extends AbstractController
{
    public function __construct(private ValidatorInterface $validator, private UserRepository $userRepository, private UserPasswordHasherInterface $passwordHasher)
    {
    }

    #[Route('', name: 'list_users', methods: ['GET'])]
    public function index(): Response
    {
        $response = new Response();
        $users = $this->userRepository->list();
        if (!empty ($users)) {
            $response->setContent(json_encode($users));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'get_user', methods: ['GET'])]
    public function show(int $id): Response
    {
        $response = new Response();
        $user = $this->userRepository->getOne($id);
        if (!empty ($user)) {
            $response->setContent(json_encode($user));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(204);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('', name: 'add_user', methods: ['POST'])]
    public function store(Request $request): Response
    {
        $user = $request->getPayload()->all();
        $response = new Response();
        
        $validation = $this->validateUserData($user);        
        if($this->validateUserData($user)->getStatusCode() == 400) {
            return $validation;
        }

        $newUser = $this->userRepository->addUser($this->passwordHasher, $user);
        if (!empty ($newUser)) {
            $response->setStatusCode(201);
        } else {
            $response->setStatusCode(400);
        }

        $response->setContent(json_encode($newUser));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'update_user', methods: ['PUT'])]
    public function update(Request $request, int $id): Response
    {
        $response = new Response();
        $user = $request->getPayload()->all(); 
        $this->userRepository->updateUser($user, $id);
        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    #[Route('/{id}', name: 'delete_user', methods: ['DELETE'])]
    public function destroy(int $id): Response
    {
        $response = new Response();
        $this->userRepository->delete($id);
        $response->setStatusCode(204);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function validateUserData(array $data): Response 
    {
        $response =  new Response();
        $constraints = new Assert\Collection([
            'name' => [
                new Assert\Type('string'),
                new Assert\Length(min: 3)
            ],
            'email' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Email()
            ],
            'password' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Length(min: 3)
            ]
        ]);

        $errors = $this->validator->validate($data, $constraints);

        if (count($errors) > 0) { 
            $messages = [];
            foreach ($errors as $k => $v) { 
                $messages[$v->getPropertyPath()] = $v->getMessage();
            }
            $response->setContent(json_encode($messages));
            $response->setStatusCode(400);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return $response;
    }
}
