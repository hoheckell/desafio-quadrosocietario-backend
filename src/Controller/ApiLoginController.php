<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class ApiLoginController extends AbstractController
{
    public function __construct(private UserRepository $userRepository,private UserPasswordHasherInterface $passwordHasher){

    }
    // #[Route('/api/login_check', name: 'api_login', methods: ['POST'])]
    // public function index(#[CurrentUser] ?User $user): Response
    // {
    //     if (null === $user) {
    //         return $this->json([
    //             'message' => 'missing credentials',
    //         ], Response::HTTP_UNAUTHORIZED);
    //     }
    //     $user = $this->userRepository->checkUser($this->passwordHasher,$user->getEmail(), $user->getPassword());
    //     if($user) {

    //     }


    //     $token = $_ENV['APP_SECRET'];
    //     return $this->json([ 
    //         'token' => $token,
    //     ]);
    // }
}
