<?php

namespace App\Repository;

use App\Dto\UserDto;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function list() :array
    {
        $listausers = [];
        $users = $this->findAll();
        if (!empty ($users)) {
            $listausers = array_map(function ($user) {
                return $user->toDto();
            }, $users);
        }
        return $listausers;
    }

    public function getOne(int $id) :UserDto
    { 
        $user = $this->find($id);
        if (!empty ($user)) { 
                return $user->toDto(); 
        } else {
            throw new NotFoundHttpException("Usuário não existe");
        }
    }

    public function addUser(UserPasswordHasherInterface $passwordHasher, array $user): UserDto
    {
        $existantUser = $this->findOneBy(["email" => $user['email']]);
        if (empty ($existantUser)) {
            $entityManager = $this->getEntityManager();
            $newUser = new User();
            $newUser->setEmail($user['email']);
            $newUser->setName($user['name']);
            $newUser->setPassword($user['password']);
            $hashedPassword = $passwordHasher->hashPassword($newUser, $user['password']);
            $newUser->setPassword($hashedPassword);
            $entityManager->persist($newUser);
            $entityManager->flush();
            return $newUser->toDto();
        } else {
            throw new \RuntimeException('User Existe');
        }
    }

    
    public function checkUser(UserPasswordHasherInterface $passwordHasher, string $email, string $password): User|bool
    {
        $user = $this->findOneBy(["email" => $email]);
        if ($user) {
            if ($passwordHasher->isPasswordValid($user, $password)) {
                return $user;
            }
        }
        return false;
    }

    public function updateUser(array $user, int $id): void
    {
        $myUser = $this->find($id);
        if ($myUser) {
            $entityManager = $this->getEntityManager();
            $myUser->setEmail($user['email'] ?? $myUser->getEmail());
            $myUser->setName($user['name'] ?? $myUser->getName());
            $entityManager->persist($myUser);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Usuário não encontrado');
        }
    }

    public function delete(int $id): void
    {
        $myUser = $this->find($id);
        if ($myUser) {
            $entityManager = $this->getEntityManager();
            $entityManager->remove($myUser);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Usuário não encontrado');
        }
    }
}
