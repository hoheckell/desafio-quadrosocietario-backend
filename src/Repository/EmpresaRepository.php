<?php

namespace App\Repository;

use App\Dto\EmpresaDto;
use App\Entity\Empresa;
use App\Entity\Socio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Void_;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @extends ServiceEntityRepository<Empresa>
 *
 * @method Empresa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Empresa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Empresa[]    findAll()
 * @method Empresa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpresaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Empresa::class);
    }

    public function list(): array
    {
        $listaempresas = [];
        $empresas = $this->findAll();
        if (!empty ($empresas)) {
            $listaempresas = array_map(function ($empresa) {
                $dto = $empresa->toDto();
                $socios = $empresa->getSocios()->toArray();
                $dto->socios = array_map(function ($socio) {
                    return $socio->toDto();
                }, $socios);
                return $dto;
            }, $empresas);
        }
        return $listaempresas;

    }
    public function getOne(int $id): EmpresaDto
    {
        $empresa = $this->find($id);
        if (!empty ($empresa)) {
            $dto = $empresa->toDto();
            $socios = $empresa->getSocios()->toArray();
            $dto->socios = array_map(function ($socio) {
                return $socio->toDto();
            }, $socios);
            return $dto;
        } else {
            throw new NotFoundHttpException("Empresa não existe");
        }

    }

    public function addEmpresa(array $empresa): EmpresaDto|bool
    {
        $existantEmpresa = $this->findOneBy(["cnpj" => $empresa['cnpj']]);
        if (empty ($existantEmpresa)) {
            $entityManager = $this->getEntityManager();
            $newEmpresa = new Empresa();
            $newEmpresa->setCnpj($empresa['cnpj']);
            $newEmpresa->setNomeFantasia($empresa['nome_fantasia']);
            $newEmpresa->setRazaoSocial($empresa['razao_social']);
            if (!empty ($empresa['socios']))
                foreach ($empresa['socios'] as $socio) {
                    $associado = $this->getEntityManager()->getRepository(Socio::class)->findOneBy(['cpf' => $socio]);
                    if (!empty ($associado))
                        $newEmpresa->addSocio($associado);
                }
            $entityManager->persist($newEmpresa);
            $entityManager->flush();
            return $newEmpresa->toDto();
        } else {
            throw new \RuntimeException('Empresa Existe');
        }
    }

    public function updateEmpresa(array $empresa, int $id): void
    {
        $myEmpresa = $this->find($id);
        if ($myEmpresa) {
            $entityManager = $this->getEntityManager();
            $myEmpresa->setCnpj($empresa['cnpj'] ?? $myEmpresa->getCnpj());
            $myEmpresa->setNomeFantasia($empresa['nome_fantasia'] ?? $myEmpresa->getNomeFantasia());
            $myEmpresa->setRazaoSocial($empresa['razao_social'] ?? $myEmpresa->getRazaoSocial());
            if (!empty ($empresa['socios']))
                foreach ($empresa['socios'] as $socio) {
                    $associado = $this->getEntityManager()->getRepository(Socio::class)->findOneBy(['cpf' => $socio]);
                    if (!empty ($associado))
                        $myEmpresa->addSocio($associado);
                }
            $entityManager->persist($myEmpresa);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Empresa não encontrada');
        }
    }

    public function delete(int $id): void
    {
        $myEmpresa = $this->find($id);
        if ($myEmpresa) {
            $entityManager = $this->getEntityManager();
            $entityManager->remove($myEmpresa);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Empresa não encontrada');
        }
    }

    public function desassociar(int $empresaId, int $socioId): void
    {
        $empresa = $this->find($empresaId);
        if (!empty ($empresa)) {
            $socio = $this->getEntityManager()->getRepository(Socio::class)->find($socioId);
            if (!empty ($socio)) {
                $empresa->removeSocio($socio);
                $entityManager = $this->getEntityManager();
                $entityManager->persist($empresa);
                $entityManager->flush();
            }
        }

    }
    public function associar(int $empresaId, array $newsocio): EmpresaDto | bool
    {
        $empresa = $this->find($empresaId);
        if (!empty ($empresa)) {
            $socio = new Socio();
            $socio->setName($newsocio['name']);
            $socio->setEmail($newsocio['email']);
            $socio->setCpf($newsocio['cpf']);
            $socio->addEmpresa($empresa);
            $entityManager = $this->getEntityManager();
            $entityManager->persist($socio);
            $entityManager->flush();
            $dto = $empresa->toDto();
            $socios = $empresa->getSocios()->toArray();
            $dto->socios = array_map(function ($socio) {
                return $socio->toDto();
            }, $socios);
            return $dto;
        } else {
            return false;
        }

    }
}
