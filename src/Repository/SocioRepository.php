<?php

namespace App\Repository;

use App\Dto\SocioDto;
use App\Entity\Socio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @extends ServiceEntityRepository<Socio>
 *
 * @method Socio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Socio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Socio[]    findAll()
 * @method Socio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private EmpresaRepository $empresaRepository)
    {
        parent::__construct($registry, Socio::class);
    }

    public function list() : array
    {
        $listasocios = [];
        $socios = $this->findAll();   
        if (!empty($socios)) {
            $listasocios = array_map(function ($socio) {
                $dto = $socio->toDto(); 
                $empresas = $socio->getEmpresas()->toArray(); 
                $dto->empresas = array_map(function ($empresa) {
                    return $empresa->toDto();
                },$empresas);
                return $dto;
            }, $socios);
        }
        return $listasocios;
    }
    public function getOne(int $id) : SocioDto
    {
        
        $socio = $this->find($id);   
        if (!empty($socio)) { 
                $dto = $socio->toDto(); 
                $empresas = $socio->getEmpresas()->toArray(); 
                $dto->empresas = array_map(function ($empresa) {
                    return $empresa->toDto();
                },$empresas);
                return $dto; 
        } else {
            throw new NotFoundHttpException("Sócio não existe");
        }
    }

    public function addSocio(array $socio): SocioDto|bool
    {
        $existantSocio = $this->findOneBy(["email" => $socio['email']]);
        if (empty ($existantSocio)) {
            $entityManager = $this->getEntityManager();
            $newSocio = new Socio();
            $newSocio->setCpf($socio['cpf']);
            $newSocio->setName($socio['name']);
            $newSocio->setEmail($socio['email']);
            if (!empty ($socio['empresas']))
                foreach ($socio['empresas'] as $empresa) {
                    $associado = $this->empresaRepository->findOneBy(['cnpj' => $empresa]);
                    if(!empty($associado))
                    $newSocio->addEmpresa($associado);
                }
            $entityManager->persist($newSocio);
            $entityManager->flush();
            return $newSocio->toDto();
        } else {
            throw new \RuntimeException('Socio Existe');
        }
    }

    public function updateSocio(array $socio, int $id): void
    {
        $mySocio = $this->find($id);
        if ($mySocio) {
            $entityManager = $this->getEntityManager();
            $mySocio->setCpf($socio['cpf'] ?? $mySocio->getCpf());
            $mySocio->setEmail($socio['email'] ?? $mySocio->getEmail());
            $mySocio->setName($socio['name'] ?? $mySocio->getName());
            if (!empty ($socio['empresas']))
                foreach ($socio['empresas'] as $empresa) {
                    $associado = $this->empresaRepository->findOneBy(['cnpj' => $empresa]);
                    if(!empty($associado))
                    $mySocio->addEmpresa($associado);
                }
            $entityManager->persist($mySocio);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Empresa não encontrada');
        }
    }

    public function delete(int $id): void
    {
        $mySocio = $this->find($id);
        if ($mySocio) {
            $entityManager = $this->getEntityManager();
            $entityManager->remove($mySocio);
            $entityManager->flush();
        } else {
            throw new NotFoundHttpException('Empresa não encontrada');
        }
    }

}
